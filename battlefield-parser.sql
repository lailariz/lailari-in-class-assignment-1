create database battlefield

create table reports(id mediumint unsigned not null auto_increment, ammunition smallint unsigned not null, soldiers smallint unsigned not null, duration double(6,1) unsigned not null, critique tinytext, posted timestamp not null default CURRENT_TIMESTAMP, primary key(id))engine = INNODB DEFAULT character SET = utf8 COLLATE = utf8_general_ci;
