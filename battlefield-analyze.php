<!DOCTYPE html>
<head>
<title>Battlefield Analysis</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>
</head>
<body><div id="main">
    <h1>Battlefield Parser</h1>
    <h2>Latest Critiques</h2>
    <?php
        $stmt = $mysqli->prepare("select ammo, soldiers, duration, critique, from battlefield order by posted desc limit 5");
        if(!$stmt){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
        }
        $stmt->execute();
        
        $result = $stmt->get_result();
        //displays all stories and comments
        echo "<div id='main'>";
        echo "<ul>\n";
        while($row = $result->fetch_assoc()){
            $ammo = htmlspecialchars($row['ammo']));
            $soldiers = htmlspecialchars($row['soldiers']);
            $duration = htmlspecialchars($row['duration'];
            $critique = htmlspecialchars($row['critique'];
            echo "ammo is ".$ammo".";
            echo "soldiers is ".$soldiers".";
            echo "duration is ".$duration".";
            echo "critique is ".$critique".";
        }
        echo "</ul>\n";
        
        stmt->close();
    ?>
    <h2>Battle Statistics</h2>
    <?php
    echo "<table>\n";
    echo "<tr> <th>rate</th> <th>soldiers</th> </tr>";
    $stmt = $mysqli->prepare("select ammo/duration as rate, from battlefield order by soldiers desc");
    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->execute();
    
    $result = $stmt->get_result();
    while($row = $result->fetch_assoc()){
        $rate = htmlspecialchars($row['rate']));
        $soldiers = htmlspecialchars($row['soldiers']);
        echo "<tr> <th>".$rate."</th> <th>".$soldiers."</th> </tr>";
    ?>
    echo "</table>\n";
    
    echo "<a href='battlefield-submit.html'> Submit a new battle report </a>";
</div></body>
</html>